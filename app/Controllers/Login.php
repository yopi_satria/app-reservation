<?php 
namespace App\Controllers;
use App\Models\M_Login;
use App\Models\M_MUser;

class Login extends BaseController
{
    protected $_dbconn;
	public function __construct(){
        //helper(['form']);
        $this->_dbconn = \Config\Database::connect('default', false); //db_connect();
	}

	public function index()
	{
		return view('v_login');
	}

	public function verification()
    {
        $session = session();
        
        $model_custom = new M_Login($this->_dbconn);
        $model = new M_MUser();
        $input = $this->request->getPost();
		$username = $input['username']; //$this->request->getVar('username'); //
        $password = $input['password']; //$this->request->getVar('password'); //
        echo "<pre>";
        var_dump($input);
        
        $datane = $model_custom->getDataUser($username);
        print_r($datane); 
        $menune = $model_custom->getUserMenu($username);
        print_r($menune);         
        $parent = $model_custom->getParentMenu_byID(11);
        print_r($parent); 
        exit;
		
		$model->where('username', $username);
		$model->where('isactive', 1);
        $data = $model->first();
        if($data){
            $dtpass = $data['password'];
            $verify_pass = password_verify($password, $dtpass);
            if($verify_pass){
                $ses_data = [
                    'user_id'       => $data['user_id'],
                    'username'      => $data['username'],
					'email'   		=> $data['email'],
					'firstname'   	=> $data['firstname'],
					'lastname'   	=> $data['lastname'],
					'client_id'   	=> $data['client_id'],
                    'logged_in'     => TRUE
                ];
                $session->set($ses_data);
                return redirect()->to('/home');
            }else{
                $session->setFlashdata('msg', 'Wrong Password');
                return redirect()->to('/login');
            }
        }else{
            $session->setFlashdata('msg', 'Username not Found');
            return redirect()->to('/login');
        }

	}

	//--------------------------------------------------------------------

}

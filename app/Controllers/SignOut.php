<?php 
namespace App\Controllers;

class SignOut extends BaseController
{
    public function index()
	{
		$session = session();
        $session->destroy();
        return redirect()->to('/login');
	}
    
}
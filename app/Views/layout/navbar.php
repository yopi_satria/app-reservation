      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
        <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">
            <li class="nav-item nav-profile">
              <a href="#" class="nav-link">
                <div class="nav-profile-image">
                  <img src="assets/images/faces/face1.jpg" alt="profile">
                  <span class="login-status online"></span>
                  <!--change to offline or busy as needed-->
                </div>
                <div class="nav-profile-text d-flex flex-column">
                  <span class="font-weight-bold mb-2">
                    <?php
                      $session = session();
                      echo $session->get('firstname'); 
                    ?></span>
                  <span class="text-secondary text-small">Project Manager</span>
                </div>
                <i class="mdi mdi-bookmark-check text-success nav-profile-badge"></i>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="index.html">
                <span class="menu-title">Dashboard</span>
                <i class="mdi mdi-home menu-icon"></i>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                <span class="menu-title">Office</span>
                <i class="menu-arrow"></i>
                <i class="mdi mdi-contacts menu-icon"></i>
              </a>
              <div class="collapse" id="ui-basic">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item"> <a class="nav-link" href="pages/ui-features/buttons.html">Company Profile</a></li>
                  <li class="nav-item"> <a class="nav-link" href="pages/ui-features/typography.html">Employee</a></li>
                  <li class="nav-item"> <a class="nav-link" href="pages/icons/mdi.html">Tax / Currency</a></li>
                  <li class="nav-item"> <a class="nav-link" href="pages/forms/basic_elements.html">Saldo Awal</a></li>
                </ul>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#product" aria-expanded="false" aria-controls="ui-basic">
                <span class="menu-title">Product</span>
                <i class="menu-arrow"></i>
                <i class="mdi mdi-cube menu-icon"></i>
              </a>
              <div class="collapse" id="product">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item"> <a class="nav-link" href="pages/samples/error-404.html">Category</a></li>
                  <li class="nav-item"> <a class="nav-link" href="pages/ui-features/typography.html">Catalog</a></li>
                </ul>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#reservation" aria-expanded="false" aria-controls="ui-basic">
                <span class="menu-title">Reservation</span>
                <i class="menu-arrow"></i>
                <i class="mdi mdi-table-large menu-icon"></i>
              </a>
              <div class="collapse" id="reservation">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item"> <a class="nav-link" href="pages/icons/mdi.html">Table</a></li>
                  <li class="nav-item"> <a class="nav-link" href="pages/ui-features/typography.html">Booked</a></li>
                </ul>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#sale" aria-expanded="false" aria-controls="sale">
                <span class="menu-title">Sale</span>
                <i class="menu-arrow"></i>
                <i class="mdi mdi-sale menu-icon"></i>
              </a>
              <div class="collapse" id="sale">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item"> <a class="nav-link" href="pages/forms/basic_elements.html">Point Of Sale</a></li>
                  <li class="nav-item"> <a class="nav-link" href="pages/charts/chartjs.html">Invoice</a></li>
                </ul>
              </div>
            </li>           
            <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#report-pages" aria-expanded="false" aria-controls="general-pages">
                <span class="menu-title">Report</span>
                <i class="menu-arrow"></i>
                <i class="mdi mdi-chart-bar menu-icon"></i>
              </a>
              <div class="collapse" id="report-pages">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item"> <a class="nav-link" href="pages/samples/blank-page.html"> Open Balance </a></li>
                  <li class="nav-item"> <a class="nav-link" href="pages/samples/login.html"> Reservation Table </a></li>
                </ul>
              </div>
            </li>
            <li class="nav-item sidebar-actions">                         
                <div class="mt-4">
                  <div class="border-bottom"></div>
                  <p class="font-weight-normal mb-3">Administrator</p>
                </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#config-pages" aria-expanded="false" aria-controls="general-pages">
                <span class="menu-title">Configuration</span>
                <i class="menu-arrow"></i>
                <i class="mdi mdi-settings menu-icon"></i>
              </a>
              <div class="collapse" id="config-pages">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item"> <a class="nav-link" href="pages/samples/blank-page.html"> Master User </a></li>
                  <li class="nav-item"> <a class="nav-link" href="pages/samples/login.html"> Master Menu </a></li>
                  <li class="nav-item"> <a class="nav-link" href="pages/samples/login.html"> Master Role </a></li>
                  <li class="nav-item"> <a class="nav-link" href="pages/samples/login.html"> Role Menu </a></li>
                  <li class="nav-item"> <a class="nav-link" href="pages/samples/login.html"> User Roles </a></li>
                </ul>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#config-pages" aria-expanded="false" aria-controls="general-pages">
                <span class="menu-title">Application Config</span>
                <i class="menu-arrow"></i>
                <i class="mdi mdi-settings menu-icon"></i>
              </a>
              <div class="collapse" id="config-pages">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item"> <a class="nav-link" href="pages/samples/blank-page.html"> Control Panel </a></li>
                  <li class="nav-item"> <a class="nav-link" href="pages/samples/login.html"> Premium Access </a></li>
                </ul>
              </div>
            </li>
           
            <li class="nav-item sidebar-actions">
              <span class="nav-link">                
                <div class="mt-4">
                  <div class="border-bottom"></div>
                  <p class="font-weight-normal mb-3">Business Categories</p>                  
                  <ul class="gradient-bullet-list mt-4">
                    <li>Restaurant</li>
                    <li>Dealer</li>
                    <li>Retail</li>
                  </ul>
                </div>
                <div class="border-bottom"></div>
                <h6 class="font-weight-normal mb-3">Premium Feature</h6>                
                <button class="btn btn-block btn-lg btn-gradient-primary mt-4">+ Get BOT WA</button>
              </span>
            </li>
            <li class="nav-item sidebar-actions">
              <p class="text-secondary">App Version 1.0</p>   
            </li>
          </ul>
        </nav>
      <!-- partial:partials/_footer.html -->
          <!-- <footer class="footer">
            <div class="container-fluid clearfix">
              <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © bootstrapdash.com 2020</span>
              <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"> Free <a href="https://www.bootstrapdash.com/bootstrap-admin-template/" target="_blank">Bootstrap admin templates </a> from Bootstrapdash.com</span>
            </div>
          </footer> -->
          <!-- partial -->
    <!-- </div> -->
    <!-- main-panel ends -->
    
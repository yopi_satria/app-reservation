<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class UserRoleSeeder extends Seeder
{
	public function run()
	{
		// membuat data		
		$user[1] = array(1,2,3,4,5,6);
		$user[2] = array(2,3,4,5,6);
		$role[3] = array(3);

		$n = 3;
		for ($i=1; $i <=3 ; $i++) { 
			$data['client_id'] = 101;
			$data['user_id'] = $i;
			$data['role_id'] = $i;
			$data['create_by'] = 'superuser';			
			$this->db->table('m_user_role')->insert($data);
		}
	}
}

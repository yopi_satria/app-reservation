<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class RoleMenuSeeder extends Seeder
{
	public function run()
	{
		// membuat data		
		$role[1] = array(11,12,13,
						21,22,
						31,32,
						41,42,43,
						51,52,53);
		$role[2] = array(21,22,
					31,32,
					41,42);
		$role[3] = array(31,32);

		$n = 3;
		for ($i=1; $i <=3 ; $i++) { 
			$data['client_id'] = 101;
			$data['role_id'] = $i;
			$data['create_by'] = 'superuser';

			foreach($role[$i] as $menu){
				// insert semua data ke tabel
				$data['menu_id'] = $menu;
				$this->db->table('m_role_menu')->insert($data);
			}

		}
		
	}
}

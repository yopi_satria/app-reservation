<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class MenuSeeder extends Seeder
{
	public function run()
	{
		// membuat data		
		$parent_menu = [[
				'client_id' => 101,
				'menu_id'  => 1,
				'nama_menu'  => 'Office',
				'path'  => null,
				'icon'  => 'mdi mdi-sale menu-icon',
				'parent_id' => null,	
			],	[
				'client_id' => 101,
				'menu_id'  => 2,
				'nama_menu'  => 'Product',
				'path'  => null,
				'icon'  => 'mdi mdi-cube menu-icon',
				'parent_id' => null,	
			],
			[
				'client_id' => 101,
				'menu_id'  => 3,
				'nama_menu'  => 'Reservation',
				'path'  => null,
				'icon'  => 'mdi mdi-table-large menu-icon',
				'parent_id' => null,		
			],[
				'client_id' => 101,
				'menu_id'  => 4,
				'nama_menu'  => 'Sale',
				'path'  => null,
				'icon'  => 'mdi mdi-sale menu-icon',
				'parent_id' => null,		
			],[
				'client_id' => 101,
				'menu_id'  => 5,
				'nama_menu'  => 'Report',
				'path'  => null,
				'icon'  => 'mdi mdi-sale menu-icon',
				'parent_id' => null,		
				],
		];
		$batch_1 = [
					
			[
				'client_id' => 101,
				'menu_id'  => 11,
				'nama_menu'  => 'Company Profile',
				'path'  => '/company',
				'icon'  => 'mdi mdi-sale menu-icon',	
				'parent_id' => 1,				
			],
			[
				'client_id' => 101,
				'menu_id'  => 12,
				'nama_menu'  => 'Employee',
				'path'  => '/employee',
				'icon'  => 'mdi mdi-sale menu-icon',
				'parent_id' => 1,		
			],	
			[
				'client_id' => 101,
				'menu_id'  => 13,
				'nama_menu'  => 'Tax & Currency',
				'path'  => '/taxcurrency',
				'icon'  => 'mdi mdi-sale menu-icon',
				'parent_id' => 1,		
			]		
		];

		$batch_2 = [
			[
				'client_id' => 101,
				'menu_id'  => 21,
				'nama_menu'  => 'Category',
				'path'  => '/product/category',
				'icon'  => 'mdi mdi-cube menu-icon',
				'parent_id' => 2,		
			],	
			[
				'client_id' => 101,
				'menu_id'  => 22,
				'nama_menu'  => 'Catalog',
				'path'  => '/product/catalog',
				'icon'  => 'mdi mdi-cube menu-icon',
				'parent_id' => 2,		
			],		
		];
		$batch_3 = [[
				'client_id' => 101,
				'menu_id'  => 31,
				'nama_menu'  => 'Table',
				'path'  => '/reservation/table',
				'icon'  => 'mdi mdi-cube menu-icon',
				'parent_id' => 3,				
			],[
				'client_id' => 101,
				'menu_id'  => 32,
				'nama_menu'  => 'Table',
				'path'  => '/reservation/Booked',
				'icon'  => 'mdi mdi-cube menu-icon',
				'parent_id' => 3,			
			],	
		];
		$batch_4 = [[
				'client_id' => 101,
				'menu_id'  => 41,
				'nama_menu'  => 'Table',
				'path'  => '/sale/pos',
				'icon'  => 'mdi mdi-cube menu-icon',
				'parent_id' => 4,		
			],[
				'client_id' => 101,
				'menu_id'  => 42,
				'nama_menu'  => 'invoice',
				'path'  => '/sale/invoice',
				'icon'  => 'mdi mdi-cube menu-icon',
				'parent_id' => 4,		
			],
			[
				'client_id' => 101,
				'menu_id'  => 43,
				'nama_menu'  => 'payment',
				'path'  => '/sale/payment',
				'icon'  => 'mdi mdi-cube menu-icon',
				'parent_id' => 4,			
			],	
		];
		$batch_5 = [[
				'client_id' => 101,
				'menu_id'  => 51,
				'nama_menu'  => 'Table',
				'path'  => '/sale/pos',
				'icon'  => 'mdi mdi-cube menu-icon',
				'parent_id' => 5,	
			],[
				'client_id' => 101,
				'menu_id'  => 52,
				'nama_menu'  => 'invoice',
				'path'  => '/sale/invoice',
				'icon'  => 'mdi mdi-cube menu-icon',
				'parent_id' => 5,	
			],
			[
				'client_id' => 101,
				'menu_id'  => 53,
				'nama_menu'  => 'payment',
				'path'  => '/sale/payment',
				'icon'  => 'mdi mdi-cube menu-icon',
				'parent_id' => 5,		
			],	
		];
		

		$batch_data =  array_merge($parent_menu,$batch_1,$batch_2, $batch_3,$batch_4,$batch_5);
		//print_r($batch_data);

		foreach($batch_data as $data){
			// insert semua data ke tabel
			$data['create_by'] = 'superuser';
			$this->db->table('m_menu')->insert($data);
		}
	}
}

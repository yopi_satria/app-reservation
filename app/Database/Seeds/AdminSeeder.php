<?php 
namespace App\Database\Seeds;
use CodeIgniter\Database\Seeder;

class AdminSeeder extends Seeder
{
        public function run()
        {
                // membuat data
                $batch_data = [
                    [
                        'client_id' => 101,
                        'user_id'  => 1,
                        'firstname'  => 'SuperUser',
                        'lastname'  => 'System',
                        'username'  => 'superuser',
                        'password'  => password_hash('system',PASSWORD_DEFAULT),
                        'email'  => 'superuser@email.com',
                        'create_by' => 'superuser'
                    ],
                    [
                        'client_id' => 101,
                        'user_id'  => 2,
                        'firstname'  => 'Admin',
                        'lastname'  => 'Administrator',
                        'username'  => 'admin',
                        'password'  =>  password_hash('admin',PASSWORD_DEFAULT),
                        'email'  => 'admin@email.com',
                        'create_by' => 'superuser'
                    ],
                    [
                        'client_id' => 101,
                        'user_id'  => 3,
                        'firstname'  => 'Kasir',
                        'lastname'  => '',
                        'username'  => 'kasir',
                        'password'  =>  password_hash('kasir',PASSWORD_DEFAULT),
                        'email'  => 'kasir@email.com',
                        'create_by' => 'superuser'
                    ],
                ];

                foreach($batch_data as $data){
                    // insert semua data ke tabel
                    $this->db->table('m_user')->insert($data);
                }



                // $data = [
                //         'username' => 'superuser',
                //         'email'    => 'superuser@app.com'
                // ];

                // // Simple Queries
                // $this->db->query("INSERT INTO users (username, email) VALUES(:username:, :email:)",
                //         $data
                // );

                // Using Query Builder
                //$this->db->table('users')->insert($data);
        }
}
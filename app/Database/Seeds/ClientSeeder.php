<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class ClientSeeder extends Seeder
{
	public function run()
	{
			
		// membuat data
		$client_data = [
			[
				'client_id' => 101,
				'nama_client'  => 'PT Demo Aplikasi',
				'token' => uniqid(),
				'create_by' => 'superuser'
			],			
		];

		foreach($client_data as $data){
			// insert semua data ke tabel
			$this->db->table('m_client')->insert($data);
		}
		//ganti properties field
		$this->db->query("ALTER TABLE m_client AUTO_INCREMENT 1000");
	}
}

<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MRoleMenu extends Migration
{
	public function up()
	{
		//field dari table m_user
		$fields = [
			'client_id'          => [
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => true,
			],
			'role_menu_id'          => [
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => true,
				'auto_increment' => true
			],
			'role_id'          => [
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => true,
			],
			'menu_id'          => [
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => true,
			],
			'isactive'          => [
				'type'           => 'INT',
				'constraint'     => 2,
				'default'       => 1,
			],	
			'iswrite'          => [
				'type'           => 'INT',
				'constraint'     => 2,
				'default'        => 1,
			],
			'ismodify'          => [
				'type'           => 'INT',
				'constraint'     => 2,
				'default'        => 1,
			],	
			'isdelete'          => [
				'type'           => 'INT',
				'constraint'     => 2,
				'default'        => 0,
			],						
			'created_at datetime default current_timestamp',		
			'create_by' => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
				'null'           => true,
			],			
			'updated_at datetime on update current_timestamp',
			'updated_by' => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
				'null'           => true,
			],		
		];
		
		//create database
		$this->forge->addField($fields);

		// Membuat primary key
		$this->forge->addKey('role_menu_id', TRUE);
		// iki foreign id
		// $this->db->disableForeignKeyChecks();
		$this->forge->addForeignKey('client_id','m_client','client_id');
		// $this->forge->addForeignKey('role_id','m_role','role_id');
		// $this->forge->addForeignKey('menu_id','m_menu','menu_id');
		// $this->db->enableForeignKeyChecks();

		// Membuat tabel news
		$this->forge->createTable('m_role_menu', TRUE);
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('m_role_menu');
	}
}

<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MUser extends Migration
{
	public function up()
	{
		//field dari table m_user
		$fields = [
			'client_id'          => [
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => true,
			],
			'user_id'          => [
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => true,
				'auto_increment' => true
			],
			'firstname'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '255'
			],
			'lastname'      => [
				'type'           => 'VARCHAR',
				'constraint'     => '255',
				'null'           => true,
			],
			'username' => [
				'type'           => 'VARCHAR',
				'constraint'     => '50',
			],
			'password' => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'email' => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
				'null'           => true,
			],
			'isactive'          => [
				'type'           => 'INT',
				'constraint'     => 2,
				'default'       => 1,
			],
			'created_at datetime default current_timestamp',		
			'create_by' => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
				'null'           => true,
			],			
			'updated_at datetime on update current_timestamp',
			'updated_by' => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
				'null'           => true,
			],		
		];

		//create field
		$this->forge->addField($fields);

		// Membuat primary key
		$this->forge->addKey('user_id', TRUE);

		$this->forge->addForeignKey('client_id','m_client','client_id');

		// Membuat tabel news
		$this->forge->createTable('m_user', TRUE);

	}

	//--------------------------------------------------------------------

	public function down()
	{
		//menghapus table jika exist
		$this->forge->dropTable('m_user');
	}
}

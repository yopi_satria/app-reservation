<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MRole extends Migration
{
	public function up()
	{
		//field dari table m_user
		$fields = [
			'client_id'          => [
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => true,
			],
			'role_id'          => [
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => true,
				'auto_increment' => true
			],
			'nama_role'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '100'
			],		
			'isactive'          => [
				'type'           => 'INT',
				'constraint'     => 2,
				'default'       => 1,
			],		
			'created_at datetime default current_timestamp',		
			'create_by' => [
				'type'           => 'VARCHAR',
				'constraint'     => '50',
				'null'           => true,
			],			
			'updated_at datetime on update current_timestamp',
			'updated_by' => [
				'type'           => 'VARCHAR',
				'constraint'     => '50',
				'null'           => true,
			],
		];

		//create database
		$this->forge->addField($fields);

		// Membuat primary key
		$this->forge->addKey('role_id', TRUE);

		$this->forge->addForeignKey('client_id','m_client','client_id');

		// Membuat tabel news
		$this->forge->createTable('m_role', TRUE);
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('m_role');
	}
}

<?php
namespace App\Models;
 
use CodeIgniter\Model;
use CodeIgniter\Database\ConnectionInterface;
 
class M_MUser extends Model{     
    protected $table = 'm_user';
    protected $primaryKey = 'user_id';
    protected $allowedFields = ['user_id','username','password','email','isactive','firstname','lastname'];

}
<?php
namespace App\Models;
 
use CodeIgniter\Model;
use CodeIgniter\Database\ConnectionInterface;
 
class M_Login {     
    protected $_table_user = 'm_user';
    protected $_db;

    public function __construct(ConnectionInterface &$db)
    {
        $this->_db =& $db;
    }

    public function getDataUser($username = '')
    {
        $builder =  $this->_db->table($this->_table_user);
        $builder->where("username",strtolower($username) );
        $builder->where("isactive",1);
        return $builder->get()->getResult();
    }

    public function getUserMenu($username = '')
    {        
        $builder = $this->_db->table('m_menu m');
        $builder->select('m.*');
        $builder->join('m_role_menu rm', 'rm.menu_id = m.menu_id');
        $builder->join('m_user_role ur', 'ur.role_id = rm.role_id');
        $builder->join('m_user u', 'u.user_id = ur.user_id');
        $builder->where('u.username', strtolower($username));
        $builder->orderBy('m.parent_id', 'ASC');
        $builder->orderBy('m.menu_id', 'ASC');

        /*
                select m.*
                from m_menu m 
                join m_role_menu rm on rm.menu_id = m.menu_id
                join m_user_role ur on ur.role_id = rm.role_id
                join m_user u on u.user_id = ur.user_id
                where u.username = '".strtolower($username)."'
                order by m.parent_id, m.menu_id
        */

        return $builder->get()->getResultArray();
    }

    public function getParentMenu_byID($menuid = '')
    {   
        //pakai named binding
        $builder = $this->_db;
        $sql = "SELECT * FROM m_menu WHERE menu_id = (select parent_id from m_menu where menu_id = :menuid:)";
        $builder = $this->_db->query($sql, ['menuid'=> $menuid] );

        return $builder->getResult();
    }
}